#include <iostream>
using namespace std;

template <class T> Min(T a, T b, T c) {
	T min=a;
	(b<min)?min=b:min;
	(c<min)?min=c:min;
	return min;
}


int main() {
	int x1=0,x2=0,x3=0;
	int x[3];
	
	cout << "\n\t\tEnter 3 numbers.";
	
	for (int fx=0; fx<sizeof(x)/sizeof(x[0]); fx++) {
		cout<<"\nNumber ("<<fx<<"): ";
		cin>>x[fx];
	}
	
	cout<<"\n\t\tThe smallest Number is: "<< Min(x[0],x[1],x[2]);
	return 0;
}
